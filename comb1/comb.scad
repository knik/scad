$fs=0.2;
color([1, 0.4, 0.1]) {
//rotate([0, 0, 45])
rotate([90, 0, 90])
  {
a1 = 2.5;
d1 = 4;
w1 = 3.5;
n1 = 45;
//n1 = 22;
//n1 = 4;
s1 = 0.6;
h1 = 6;

   for(i=[0:1:n1] )
   {
     translate([0, -0.5, 0])
     rotate([a1, 0, 0])
     scale([1.0,1.0] )
       {
     translate([4*i, 0, 3])
     cylinder(26, w1/2, 0.6, center=true);

     translate([d1*i, 0, 16])
     sphere(s1, $fs=0.2);
    }
           
   }

   translate([d1*n1/2-0, 0, -10])
    scale([1,0.7,1.6] )
     rotate([45, 0, 0])
      cube([n1*d1-0, w1, w1], center=true);

   translate([d1*n1/2-0, 0, -13.0])
    cube([n1*d1-4, w1, h1], center=true);

   translate([n1*d1/2, 0, -16])
    rotate([0,90])
    cylinder(n1*d1-4, w1/2, w1/2, center=true);

  translate([2, 0, -16])
   scale([3.5, 1, 1])
   minkowski()
   {
    sphere(w1/2);
    cube([0.001, 0.001, 6]);
   }

  translate([n1*d1-2, 0, -16])
   scale([3.5, 1, 1])
   minkowski()
   {
    sphere(w1/2);
    cube([0.001, 0.001, 6]);
   }


  }
}
