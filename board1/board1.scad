// 235x235
// 27.5x117.5
// 27.5x117.5
// 180x27.5


   $fs=0.2;

   t1 = 0.2;
   t2 = 0.4;
   d1 = 27.5;
   d2 = 117.5;
   d3 = 180;

W1 = 1;
//W2 = 1;
//W3 = 1;


color([1, 0.4, 0.1]) {
//rotate([180, 0, 0])
rotate([0, 0, 90])
  {
  if (!is_undef(W1)) {
   translate([0, 0, 0])
    cube([d1, d1, t1], center=true);
  }
  if (!is_undef(W2)) {
   translate([0, 0, 0])
    cube([d2, d2, t1], center=true);
  }
  if (!is_undef(W3)) {
   translate([0, 0, 0])
    cube([d3, d3, t1], center=true);
  }
 }
}


