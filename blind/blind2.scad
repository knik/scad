module catch1 (a1) {
  translate(a1+[4.0, 0, 1])
    cube([1.4, 1.8, .2], center=true);
  translate(a1+[4.0, 0, .5])
    cube([1.0, 1.4, 1], center=true);
}
module catch2 (a1) {
  translate(a1 + [-4.0, 0.0, 0.0])
     cube([1.8, 2.8, 10], center=true);
  translate(a1 + [-1.8, 0, 0.0])
     cube([1.2, 2.0, 10], center=true);
  translate(a1 + [-3.0, 0.3, 0.0])
     cube([2.0, 1.4, 10], center=true);
}

color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
   d1 = 30;
   //d2 = 206;
   d2 = 16;

   //d1 = 10;
   //d2 = 20;
      
   difference()
   {
     union()
     {
       cube([d2, 2*d1 + 10, 0.2], center=true);

       catch1([-d2/2, d1, 0]);
       catch1([-d2/2, -d1, 0]);

  //     catch1([-d2/8, -d1/6, 0]);
  //     catch1([-d2/8, d1/6, 0]);
     }
  //   catch2([d2/2, d1, 0]);
  //   catch2([d2/2, -d1, 0]);

     d4=0.6;
     translate([2, 0, 0])
     cylinder(10, d4, d4, center=true, $fs=0.2);
     translate([20, 0, 0])
     cylinder(10, 14, 14, center=true, $fs=0.2);
//     translate([-20, 0, 0])
//     cylinder(10, 14, 14, center=true, $fs=0.2);
  };
  }
}

