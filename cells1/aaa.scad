module cell1 (d, p) {
   d1=2;
   translate([0, 0, p])
   difference()
   {
    cylinder(1, d/2, d/2, center=true, $fs=0.2);
    cylinder(500, d1/2, d1/2, center=true, $fs=0.2);
   };
}


color([1, 0.4, 0.1]) {
//rotate([180, 0, 0])
rotate([0, 90, 0])
  {
   t1=0.8;
   d=10;
   l=42;
//   translate([0, 0, 11])
   difference() {

   union() {
   difference()
   {
    cylinder(l, d/2, d/2, center=true, $fs=0.2);
    cylinder(500, d/2-t1, d/2-t1, center=true, $fs=0.2);
   };
   cell1(d, 21);
   cell1(d, -21);
   }
   translate([-25, 0, 0])
    cube([50, 50, 50], center=true);


   }
      
  }
}


