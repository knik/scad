//c1=12.6;
cells=5;
d1=2.54;
c1=cells*d1+2.6;
c2=3.2;
c3=20;
c5=0.85;

module part1 (a1) {
   difference()
   {
     translate([0, 0, 0])
     cube([c1+2, c2+2, c3+2], center=true);

     translate([0, 0, 0])
     cube([c1, c2, c3+10], center=true);
   };
   difference()
   {
     translate([0, 0, 12])
     cube([c1+0, c2+0, 4], center=true);

     translate([0, 0, 4])
     cube([c1-4, c2-1, c3+10], center=true);
   };
   difference()
   {
     translate([0, 0, 13])
     cube([c1-4, c2+0, 8], center=true);

     translate([0, 0, 4])
     cube([c1-5, c2-1, c3+10], center=true);
   };
}

module sep1() {
   translate([0, 0, 0])
   cube([d1, c5/2, c1], center=true);

   translate([d1/2-c5/4, d1*0.4, 0])
   cube([c5/2, d1, c1], center=true);

   translate([-d1/2+c5/4, d1*0.4, 0])
   cube([c5/2, d1, c1], center=true);
}


color([1, 0.4, 0.1]) {
  {


if (0) {
rotate([90, -90, 0])
 {

   difference()
   {
     part1(0);

     translate([25, 0, 0])
     cube([50, 50, 50], center=true);
   };

 }
}

if (1) {
rotate([90, 0, 0])
    {
   for(i=[0:1:cells-1] )
   {
     translate([d1*i, 0, 0])
     sep1();
   }
   }
}


  }
}


