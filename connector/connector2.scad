c1=10;
c2=3.2;
c3=20;
c5=0.85;

module part1 (a1) {
   difference()
   {
     translate([0, 0, 0])
     cube([c1+2, c2+2, c3+2], center=true);

     translate([0, 0, 0])
     cube([c1, c2, c3+10], center=true);
   };
   difference()
   {
     translate([0, 0, 12])
     cube([c1+0, c2+0, 4], center=true);

     translate([0, 0, 4])
     cube([c1-4, c2-1, c3+10], center=true);
   };
   difference()
   {
     translate([0, 0, 13])
     cube([c1-4, c2+0, 8], center=true);

     translate([0, 0, 4])
     cube([c1-5, c2-1, c3+10], center=true);
   };
}


color([1, 0.4, 0.1]) {
  {


if (0) {
rotate([90, -90, 0])
 {

   difference()
   {
     part1(0);

     translate([25, 0, 0])
     cube([50, 50, 50], center=true);
   };

 }
}

if (1) {
rotate([90, 0, 0])
    {
   difference()
   {
     translate([0, 0, 0])
     cube([c1, c2, c3-5], center=true);

     translate([0, 0, 0])
     cube([c1-c5, c2-c5, c3+10], center=true);
     translate([0, 11, 0])
     cube([20, 20, c3+10], center=true);
   };
   translate([c1/6, 0, 0])
   cube([c5/2, 2.4, c3-5], center=true);
   translate([-c1/6, 0, 0])
   cube([c5/2, 2.4, c3-5], center=true);
   }
}

 
  }
}


