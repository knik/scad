module scr1 (a1) {
  for(i=[0:1:18] )
   {
    rotate([0, 0, 6+a1+10*i])
    translate([19.2, 0, 0.0+i*0.2])
    cube([3, 3.4, 0.8], center=true); 
   }
}


color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
   translate([0, 0, 2])
   scr1(0);
   translate([0, 0, 2])
   scr1(180);

   translate([0, 0, 5])
   difference()
   {
     cylinder(8, 21, 21, center=true, $fs=0.2);
     cylinder(30, 19.8, 19.8, center=true, $fs=0.2);
   };

   translate([0, 0, 8.4])
   difference()
   {
   cylinder(1.2, 21, 21, center=true, $fs=0.2);
   cylinder(20, 2, 3, center=true, $fs=0.2);
   }

   //cylinder(12, 9, 2, center=true, $fs=0.2);


   translate([0, 0, 13.8])
   difference()
   {
    cylinder(12, 3, 2, center=true, $fs=0.2);
    cylinder(20, 3, 1, center=true, $fs=0.2);
   }
   
   
  }
}


