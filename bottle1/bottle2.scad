module scr2(a1) {
  for(i=[0:1:18] )
   {
    rotate([0, 0, 6+a1+10*i])
    translate([17.4, 0, 0.0+i*0.2])
    cube([2, 3.4, 0.8], center=true);
   }
}

module lid1(w, h) {
 for(i=[0:1:20] )
   {
     translate([0, 0, h+i*0.2])
     difference()
     {
       cube([(i+2)*w, (i+2)*w, 1], center=true);
       cube([i*w, i*w, 10], center=true);
     }
     /*
     for(a=[0:1:1])
     {
       rotate([0, 0, a*90])
       translate([0, 0, 0.4+h+i*0.2])
       cube([(i+2)*w, 0.2, 0.2], center=true);
     }
     for(a=[0:1:1])
     {
       rotate([0, 0, 45+a*90])
       translate([0, 0, 0.4+h+i*0.2])
       cube([(i+2)*1.2*w, 0.2, 0.2], center=true);
     }
     */

   }

/*

     for(a=[0:1:1])
     {
       rotate([0, 0, a*90])
       translate([0, 0, 6])
       cube([(20)*w, 0.2, h+6], center=true);
     }

     for(a=[0:1:1])
     {
       rotate([0, 0, 45+a*90])
       translate([0, 0, 6])
       cube([28*w, 0.2, h+6], center=true);
     }
*/

}

module body1(w, h) {
   difference()
   {
     cube([w+1, w+1, h+1], center=true); 
     cube([w, w, h+10], center=true); 
   };
}

color([1, 0.4, 0.1]) {
rotate([180, 0, 0])
  {
   scr2(0);
   scr2(180);

   translate([0, 0, 3])
   difference()
   {
     cylinder(10, 17, 17, center=true, $fs=0.2);
     cylinder(20, 16, 16, center=true, $fs=0.2);
   };

   h1 = 40;
   translate([0, 0, h1/2+8])
   body1(40, h1);

   d1 = 1.88;
   //d1 = 2.8;
   difference()
   {
     lid1(d1, 4);
     translate([0, 0, 6])
     cylinder(6, 17, 17, center=true, $fs=0.2);
   }
   lid1(d1, h1+5);

  }
}
