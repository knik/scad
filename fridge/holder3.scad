module hold1(pos)
{
thick=1;

g1=0.2;
d1=thick*2;
w=9+g1;
h=4;

w2=3;
l3=25;

w3=l2-1;
l4=40;
h2=1;
h3=1.8;

  difference()
  {
   cube([w+d1, l+d1, h+d1], center=true);

   translate([0, -l/2, -d1])
   cube([20, 0, 8], center=true);

   translate([0, -d1, -d1])
   cube([w, l+2*d1, h+2*d1], center=true);
  }

  translate([1, -l/2+h2/2-1, l2/2+2])
  cube([w2, h2, l2], center=true);

  translate([pos/2-l4/2, -l/2-0.5, w3/2+3])
  cube([pos+w/2+l4, h3, w3], center=true);

  translate([11, 1-l/2, 2])
  cube([12, 4, 8], center=true);
  translate([-11, 1-l/2, 2])
  cube([12, 4, 8], center=true);
}

module grid1(x, y)
{
  for(j=[0:1:y])
  for(i=[0:1:x])
   {
    translate([20*j-29, 0, 20*i+18])
    cube([17, 20, 17], center=true);
   }

}

l2=171;
//l2=28;
//l=40;
l=40;
//l=10;

color([1, 0.4, 0.1]) {
rotate([90, 0, 90])
{
th1=0.8;
th2=2.2;
th3=1.6;
g1=1;
s1=(g1<2) ? 20 : 0;

  mirror([0,0,1])
  {
   translate([0, l/2-5, 0])
   difference()
   {
    hold1(22);
    translate([0, -l/2-4, -2])
    grid1(7, 2);
    for(i=[0:1:2])
    {
     translate([i*26-36, 0, l2-3])
     cube([3, 80, 3], center=true);
    }
   }
   difference()
   {
    rotate([0, 0, 90])
    translate([15-s1/2, 42, l2/2+2.5])
    cube([42-s1, th2, l2-1], center=true);

    rotate([0, 0, 90])
    translate([13, 42, -2])
    grid1(7, g1);

    for(i=[0:1:g1])
    {
     if (s1<10)
     {
       translate([-10, i*15, l2-3])
       cube([80, 3, 3], center=true);
     }
     if (s1>10)
     {
       translate([-10, i*15-2, l2-3])
       cube([80, 3, 3], center=true);
     }
    }
   }
   difference()
   {
    rotate([-90, 0, 0])
    translate([-9, -3, 15-s1/2])
    cube([66, th1, 42-s1], center=true);

    rotate([-90, 0, 0])
    translate([-1, 0, -14])
    grid1(g1-1, 2);
   }

  difference()
  {
    rotate([0, 0, 90])
    translate([15-s1/2, -24, l2/2+2.5])
    cube([42-s1, th3, l2-1], center=true);

    rotate([0, 0, 90])
    translate([13, -22, -2])
    grid1(7, g1);

    for(i=[0:1:g1])
    {
     translate([-10, i*15-2, l2-3])
     cube([80, 3, 3], center=true);
    }
  }

  }


}
}

