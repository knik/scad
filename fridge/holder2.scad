color([1, 0.4, 0.1]) {
rotate([90, 0, 90])
{
thick=1;

g1=0.2;
d1=thick*2;
w=9+g1;
l=40;
h=4;

l2=171;
w2=6;
h2=8;
l3=25;

  translate([0, l/2-12, 0])
  difference()
  {
   cube([w+d1, l+d1, h+d1], center=true);

   translate([0, -l/2, -d1])
   cube([20, 0, 8], center=true);

   translate([0, -d1, -d1])
   cube([w, l+2*d1, h+2*d1], center=true);
  }

  difference()
  {
    translate([0, h2/2-13, l2/2+2])
    cube([w2, h2, l2], center=true);

    translate([0, -h2-1, l2-l3/2-2])
    cube([2, 50, l3], center=true);
  }
}
}

