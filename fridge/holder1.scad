module spike1(w, l)
{
   scale([w, w, l])     
    {
  translate([0, 0, -0.73]) {
  difference()
   {
    translate([0, 0, 1.4])
    rotate([0, 0, 45])
     cube([2, 2, 1.4], center=true);
       
   for(i=[0:1:12] )
   {
    rotate([0, 0, i*30])
    translate([1, 0, 1])
    rotate([0, 45, 0])
    cube([1, 1, 4], center=true);
   }
  }
  }
  }
}


color([1, 0.4, 0.1]) {
rotate([180, 0, 0])
{
   //rotate([0, 0, 126])
   translate([0, 0, 0])
   cube([8.6, 24, 6], center=true);

   l1=2.7;
   l2=10;
   s1=0.9;
   s2=5;
   sx=1.8*s2;

   translate([l1, l2, -sx])
   spike1(s1, s2);

   translate([l1, -l2, -sx])
   spike1(s1, s2);

   translate([-l1, l2, -sx])
   spike1(s1, s2);

   translate([-l1, -l2, -sx])
   spike1(s1, s2);

   translate([0, 0, -3.5])
   cube([20, 40, 1], center=true);



}
    
}


