color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
   // PART A: arch
   //arch = 1;
   L = 1;
   // PART B: body
   //body = 7.1; // voicecoil radius
   //body = 0; // voicecoil radius
   // PART C: relief
   relief = 1;
   // PART D: lid
   //lid = 3.4;
   // PART E: angle appendix
   //angle = 10;

   grid = 0.2;
   d1 = 10;
   d2 = 1.8;
   m1 = 6;
   d2m = 2.4;
   d3 = 10;
   r1 = 20.2;
   h1 = 11;
   d4 = 2;

   if (!is_undef(arch)) {
   for(i=[0:1:34])
    {
      rotate([0, 0, i*7.9])
      translate([70, 0, 0])
      cube([d2, d3, d1], center=true);

      s = 2.0;
      difference() {
        rotate([0, 0, i*7.9])
        translate([70-d2/2+s/2, 0, 6])
        cube([s, d3, 2], center=true);

        rotate([0, 0, i*7.9])
        translate([70-d2/2+s/2, 0, 7])
        cube([.8, d3+1, 2+1], center=true);
      }
    }
    rotate([0, 0, 34*7.9])
    translate([69.1+d2m/2, (m1*d3/2) + 4, 0])
    cube([d2m, d3*m1, d1], center=true);

    rotate([0, 0, 0])
    translate([69.1+d2m/2, -(m1*d3/2) - 4, 0])
    cube([d2m, d3*m1, d1], center=true);
   }

   if (!is_undef(body)) {
    d1 = 4;
    difference() {
      translate([0, 0, 0])  
      cylinder(h1+d4, r1+d4, r1+d4, center=true, $fs=grid);

      translate([0, 0, 0])  
      cylinder(h1+10, r1, r1, center=true, $fs=grid);

      translate([20, 0, 2+d4])
       rotate([0, 90, 0])
       cube([6, d1, 10], center=true, $fs=grid);
      if (!is_undef(L)) {
       translate([-20, 0, 2+d4])
        rotate([0, 90, 0])
        cube([6, d1, 10], center=true, $fs=grid);
      }
    }

    difference() {
      translate([0, 0, -h1/2])  
      cylinder(d4, r1+5, r1+5, center=true, $fs=grid);

   dv = 2;
   for(j=[0:1:6])
   for(i=[0:1:6])
    {
      if ((i*i+j*j) < 40)
      {
        translate([i*3, j*3, 0])
        cube([dv, dv, 20], center=true);
        translate([-i*3, j*3, 0])
        cube([dv, dv, 20], center=true);
        translate([-i*3, -j*3, 0])
        cube([dv, dv, 20], center=true);
        translate([i*3, -j*3, 0])
        cube([dv, dv, 20], center=true);
      }
    }
    }
    translate([0, 0, -h1/2])
      cylinder(d4, body, body, center=true, $fs=grid);
   }

   if (!is_undef(relief)) {
     d = 30;
     d1 = 10;
     l = .8;
     s = .6;
     for(i=[1:1:54])
      {
        rotate([0,0,i*20])
        translate([1.2, 0, -i/5+10])
        cube([s, l, (i+d1)/d], center=true);

        rotate([0,0,-i*20])
        translate([1.2, 0, -i/5+10])
        cube([s, l, (i+d1)/d], center=true);
      }
   }

   if (!is_undef(lid)) {
     d = 1.2;
     r = r1-0.2;
     difference() {
       translate([0, 0, -1+d/2])
       cylinder(d, r, r, center=true, $fs=grid);

       x = 9;
       translate([0, 0, 0])
       cylinder(10, x, x, center=true, $fs=grid);
     }
     translate([0, 0, lid/2-1])
     difference() {
       r = 9;
       cylinder(lid, r, r, center=true, $fs=grid);

       r2 = 8;
       cylinder(20, r2, r2, center=true, $fs=grid);
     }
     translate([0, 0, 1])
     difference() {
       cylinder(2, r, r, center=true, $fs=grid);

       r2 = r1-1;
       cylinder(20, r2, r2, center=true, $fs=grid);
     }

   }

   if (!is_undef(angle)) {
     r = 7.94;
     r2 = 7;
     t = 2.2;
     d12 = d1+0.2;
     translate([0, 0, t])
     difference() {
       cylinder(6, r, r, center=true, $fs=grid);

       cylinder(20, r2, r2, center=true, $fs=grid);

       translate([0, 0, t+.8])
       rotate([0, angle, 0])
         cube([d12, 20, 2], center=true, $fs=grid);
     }

     translate([0, 0, 5.5])
     rotate([0, angle, 0])
     difference() {
       cube([d1+2.2, 16, d2+2.4], center=true, $fs=grid);
       cube([d12, 20, d2m+.2], center=true, $fs=grid);

       cube([6, 20, 20], center=true, $fs=grid);
     }
     difference() {
       translate([0, 0, 2.4])
        cylinder(0.4, r+.2, r+.2, center=true, $fs=grid);
       cylinder(20, r2, r2, center=true, $fs=grid);
     }
   }


 }
}

