color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
{
 d1 = 0.3;
 d2 = 1;
 r1 = 18.2;
 r2 = 30;

 difference()
 {
   union()
   {
     translate([0, 0, 0.4])
     difference()
     {
       sphere(r2-2*d1);
       sphere(r2-3*d1);
       translate([0, 0, -74.2])
         cube([200, 200, 100], center=true);
       translate([0, 0, 50.2])
       //translate([0, 0, 16])
         cube([200, 200, 50], center=true);
     };

     h1 = 26;
     d4 = 1.8;
     difference()
     {
       translate([0, 0, -h1/2-11])
       difference()
       {
         cylinder(h1-23, r1-d2, r1-d2, center=true, $fs=0.2);
         cylinder(8, r1-d4, r1-d4, center=true, $fs=0.2);
       };
     }

     d3 = 1.8;
     difference()
     {
       translate([0, 0, -h1])
       difference()
       {
         cylinder(1, r1, r1, center=true, $fs=0.2);
         cylinder(8, r1-d3, r1-d3, center=true, $fs=0.2);
       };
     }
    }
 
/*  
// top cutout
   c1 = 14.4;
   translate([0, 0, 40])
     cylinder(100, c1, c1, center=true, $fs=0.2);
*/

   for(i=[0:1:5] )
   {    
     rotate(60*i)
     translate([10, 0, -20])
     if (i)
       cube([20, 6, 8], center=true);
     else
       cube([20, 4, 20], center=true);
   }
 }
}
}

