module scr1 (a1) {
  for(i=[0:1:18] )
   {
    rotate([0, 0, 6+a1+10*i])
    translate([19.0, 0, 0.0+i*0.2])
    cube([3, 3.4, 0.8], center=true); 
   }
}


color([1, 0.4, 0.1]) {
//rotate([180, 0, 0])
rotate([0, 90, 0])
  {


   s1=1.814;
   s2=0.6;
//   r1=6.35;
   s3=1.4;
   r1=6.3;
   r2=r1-s3;


   translate([0, 0, 11])
   difference()
   {
     cylinder(23, r1, r1, center=true, $fs=0.1);
     cylinder(50, r2, r2, center=true, $fs=0.1);
   };

   for(i=[0:1:170] )
   {
    rotate([0, 0, 24*i])
    translate([r1, 0, 1+i*s1/15])
    rotate([0, 45, 0])
    cube([s2, 3, s2], center=true);
   }
  }
}


