   $fs=0.2;

   //lid = 0; // voicecoil radius
   //body = 0; // voicecoil radius
   clip = 0; // voicecoil radius

//r1=35.8*0.5;
//r1=37.5*0.5;
//r1=37.6*0.5;
r1=38.8*0.5;
d1=1.6;

//r2_out=27.5
//r2=25.6/2;
//r2=27.5/2;
r2=27.8/2;
d2=1.3;

th1=1.10;

module scr1 (a1) {
  d1=1.6;
  dr0 = 3.0;
  for(i=[0:1:18] )
   {
    rotate([0, 0, 6+a1+10*i])
//    translate([r1, 0, 0.4+i*0.2])
    translate([r1, 0, 0.8+i*0.027778*dr0*2])
    rotate([0, 45, 0])
    cube([d1*th1, 3.6, d1*th1], center=true);
   }
}

module scr3 (a1) {
  d1=1.4;
  dr0 = 3.0;
  for(i=[0:1:12] )
   {
    rotate([0, 0, 6+a1+10*i])
//    translate([r1, 0, 0.4+i*0.2])
    translate([r1, 0, 0.8+i*0.027778*dr0*3])
    rotate([0, 45, 0])
    cube([d1*th1, 3.4, d1*th1], center=true);
   }
}


color([1, 0.4, 0.1]) {
//color([.3, .6, 0.3]) {
rotate([180, 0, 0])
  {
   if (!is_undef(body)) {

   if (1) {

//   scr1(0);
//   scr1(180);
   scr3(0);
   scr3(120);
   scr3(240);

//   translate([15, 0, 3.8-3*1])
//    cube([5.0, 0.1, 0.1], center=true); 

   translate([0, 0, 4])
//   translate([0, 0, 2])
   difference()
   {
     cylinder(10, r1+d1, r1+d1, center=true);
     cylinder(30, r1, r1, center=true);
//     cylinder(6, r1+d1*0.5, r1+d1*0.5, center=true);
//     cylinder(30, r1, r1, center=true);
   };

   translate([0, 0, 7])
   difference()
   {
//     cylinder(4, 16.2, 16.2, center=true);
//     cylinder(30, 15.6, 15.6, center=true);
   };

   }

   if (0) {

   for(i=[0:1:35] )
   {    
    rotate(6+12*i)
    translate([r1+d1/2, 0, -1.0])
    rotate([90, 0, 0])
    cylinder(4.6, 0.8, 0.8, center=true);
   }


  }


  {
   

   translate([0, 0, 8])
   difference()
   {
     cylinder(2, r1+d1, r1+d1, center=true);
     cylinder(20, r2, r2, center=true);
   };

// 2nd ring
///*

   translate([0, 0, 2.5])
   difference()
   {
     cylinder(13, r2+d2, r2+d2, center=true);
     cylinder(20, r2, r2, center=true);
   };

   for(i=[0:1:40] )
   {
    rotate([0, 0, 6+12*i])
    translate([r2, 0, 1+i*0.09])
    rotate([0, 45, 0])
    cube([d2*th1, 3.2, d2*th1], center=true);
   }
//*/
  }




  /*/

    r3 = 3;
    r4 = 15.2;
    s1 = 1.20;
    s2 = 12;
    translate([0, 0, -4])
    difference() {
    for(i=[0:1:14] )
    {
     translate([0, 0, 1.00*i])
      difference() {
        cylinder(1.0, r4-i*s1, r4-i*s1,
          center=true);
        cylinder(10, r4-2-i*s1, r4-2-i*s1,
          center=true);
      }
    }
      translate([s2, 0, 0])
      cylinder(20, r3, r3, center=true);

      translate([0, s2, 0])
      cylinder(20, r3, r3, center=true);

      translate([-s2, 0, 0])
      cylinder(20, r3, r3, center=true);

      translate([0, -s2, 0])
      cylinder(20, r3, r3, center=true);
    }
    translate([0, 0, 8.7])
    cube([1, 12, 0.6], center=true);
    translate([0, 0, 8.7])
    cube([12, 1, 0.6], center=true);
*/
  }
  }

  if (!is_undef(lid)) {
   translate([0, 0, -7])
   cylinder(0.6, 18, 18, center=true);
 
   for(i=[0:1:10] )
   {
     rotate([0, 0, 12*i])
     translate([17, 0, -6.6+i*0.15])
     cube([0.4, 4, 0.5], center=true); 
    }
    rotate([0, 0, 126])
    translate([16.2, 0, -5])
    cube([2, 0.5, 0.5], center=true);
  };

  if (!is_undef(clip)) {
    d1o = 0.10;
    difference()
    {
      cylinder(8, r1+d1+1+d1o,
        r1+d1+1+d1o, center=true);
      cylinder(20, r1+d1+d1o, r1+d1+d1o,
        center=true);
    };

 
  };
  
}


