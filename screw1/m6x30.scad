color([1, 0.4, 0.1]) {
//rotate([180, 0, 0])
  {

   s1=1;
   s2=0.5;
   r1=2.3;
   r2=4.6;
   f1=1.1;


if (1)
{
   translate([0, 0, 11])
   cylinder(30, r1, r1, center=true, $fs=0.1);

   difference() {
    translate([0, 0, -6])
    cylinder(5, r2*f1, r2*f1, center=true, $fs=0.1);

   for(i=[0:1:3])
   {
    translate([0, 0, -8])
    rotate([0, 0, i*60])
    cube([3, 5.4, 4], center=true);
   }
   }


   for(i=[0:1:150])
   {
    rotate([0, 0, 24*i])
    translate([r1-0.5, 0, 15.5+i*s1/15])
    scale([2,1,1])
    rotate([0, 45, 0])
    cube([s2, 3, s2], center=true);
   }
}
// washer
if (0)
{
   wfs=1.3;
   wfb=2.5;

   difference() {

    cylinder(2, r1*wfb, r1*wfb, center=true, $fs=0.1);

    cylinder(10, r1*wfs, r1*wfs, center=true, $fs=0.1);
   }

}


  }
}


