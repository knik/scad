color([1, 0.4, 0.1]) {
//rotate([-a1+90, 0, 0])
  {
    
  difference() {
    union() {
      intersection() {
          pipe1(80, 13, s1);
          translate([0, 8, 22])
          sphere(13);
          }
      pipe1(80, 12, s1+6);
      cube([11.4, 11.4, 44], center=true);
      translate([0, 5, 16])
      sphere(11);
    }

    for(i=[0:1:3])
    {
     rotate([90, 0, -90])
      translate([0, -5*i-2, -10])
       cylinder(10, 1, 1, $fs=0.2);
    }

    cube([8.4, 8.4, 100], center=true);

    pipe1(120, 10.4, s1);

    translate([0, 0, 43])
    rotate([a1, 0, 0])
    cube([200, 34, 200], center=true);

    translate([0, 0, 92])
    cube([100, 200, 100], center=true);
  }

  }
}
        
//a1=-67;
//a1=-65;
//a1=-62;
a1=-60;
s1=48;

module pipe1 (v1, v2, s) {
  rotate([a1, 0, 0])
  translate([0, -19, s])
  cylinder(v1, v2, v2,
    $fs=0.1, center=true);
}

//
