color([1, 0.4, 0.1]) {
{
  //rotate([12, 0, 0])
  rotate([180, 0, 0])
    {

a1=10;
a2=36;

  rotate([0, 0, 190]){
  difference() {
    union() {
        translate([0, 0, -10])
        cylinder(20, 9, 9, $fs=0.1);

        translate([0, -3, -3])
        cube([20, 6, 6]);

        translate([0, -5, -5])
        cube([18, 10, 10]);

        translate([0, -4, -4])
        cube([25, 8, 8]);
        

        rotate([0, 0, -a1]){
          translate([24, 1, -3])
          cube([12, 6, 6]);
        }
    }
    translate([0, 0, -11])
    cylinder(30, 6.2, 6.2,
    $fs=0.1);
  }
  }


  translate([7, -5, -4])
  cube([30, 10, 8]);
  
  translate([7, -5, -5])
  cube([24, 10, 10]);
  
  translate([26, -10, -6.5])
  cube([0.8, 20, 13]);

  translate([35, -4, -4])
  rotate([0, 0, a2])
  cube([15, 10, 8]);

  difference() {
   translate([55, 20, 0])
   rotate([0, 0, 45])
   cube([36, 8, 8], center=true);

   translate([55, 20, 0])
   rotate([0, 0, 45])
   cube([46, 6, 6], center=true);

   translate([60, 25, -10])
    rotate([0, 0, a1+a2])
     cylinder(10, 1, 1, $fs=0.2);
  }
}
}
}
        
//
