color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
   // BOTTOM:
   bottom = 1;
   // TOP:
   //top = 1;
      
      
  h1 = 0.6;
  n1 = 18;
//  n1 = 1;

  d1 = 10+n1*4;
  d2 = 90;
//  d2 = 10;
  d3 = 10;
  d4 = 6;

  if (!is_undef(bottom)) {
    difference() {
    {
     translate([0, 0, 0])
     cube([d1+1, d2, d3], center=true);
    }
    translate([0, 0, h1+5])
    cube([d1-2*h1, d2-2*h1, d3+10], center=true);
    }

    //for(j=[0:1:d2/16])
    for(i=[0:1:n1])
    {
//      translate([i*4-d1/2+5, j*16-d2/2+5, 0])
      translate([i*4-d1/2+5, 0, 0])
//      cube([h1, 10, 10], center=true);
      cube([h1, d2, 10], center=true);
    }

    d = .2;
    translate([0, d2/2+d/2, d3/2-2*d])
    cube([d1-10, d, d*3], center=true);

    translate([0, -d2/2-d/2, d3/2-2*d])
    cube([d1-10, d, d*3], center=true);
  }


  if (!is_undef(top)) {
    d0 = 2;
    difference() {
     translate([0, 0, 0])
     cube([d1+3*h1+1, d2+3*h1, d0], center=true);


     translate([0, 0, h1+5])
     cube([d1+h1+1, d2+h1, d0+10], center=true);
    }

    d = .2;
    translate([0, d2/2+d, 0.8])
    cube([d1-10, d, d*2], center=true);
    translate([0, -d2/2-d, 0.8])
    cube([d1-10, d, d*2], center=true);
  }


 }
}

