color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
   // BOTTOM:
   bottom = 1;
   // TOP:
   //top = 1;
      
      
  h1 = 0.6;

  d1 = 135;
  d2 = 42;
//  d1 = 20;
//  d2 = 20;
//  d3 = 5;

  d3 = 25;
//  d3 = 33;

/*
  d1 = 135;
  d2 = 38;
  //d3 = 25;
  d3 = 23;
*/


  d4 = 6;

  if (!is_undef(bottom)) {
    d0 = 4;
    difference() {
      translate([0, 0, 0])
      cube([d1+1*h1, d2+1*h1, d3+1*h1],
        center=true);

      translate([d0/2, 0, d0/2])
      cube([d1-h1+d0, d2-h1, d3+d0-h1], center=true);

    }
    translate([d1*0.5, 0, -d3*0.5+1.0])
    cube([h1, d2, 2], center=true);
  }


  if (!is_undef(top)) {
    g0 = 0.6;
    d0 = 2.6;
    h2 = h1+1.0;
    difference() {
     translate([0, 0, 0])
     cube([d1+3*h1, d2+3*h1, d0], center=true);

     translate([0, 0, h1+5])
     cube([d1+h1+0, d2+h1, d0+10], center=true);


/*
     for(i = [-4: 1: 4]) {
      translate([i * 20, 0, -d0*0.5+0.1])
      cube([g0, 200, 0.2], center=true);
      translate([0, i * 20, -d0*0.5+0.1])
      cube([200, g0, 0.2], center=true);

      translate([i * 20 + 5, 0, -d0*0.5+0.3])
      cube([g0, 200, 0.2], center=true);
      translate([0, i * 20 + 5, -d0*0.5+0.3])
      cube([200, g0, 0.2], center=true);

      translate([i * 20 + 10, 0, -d0*0.5+0.5])
      cube([g0, 200, 0.2], center=true);
      translate([0, i * 20 + 10, -d0*0.5+0.5])
      cube([200, g0, 0.2], center=true);
    }
*/

     }
     /*
     for(i = [-6: 1: 6]) {
     for(j = [-2: 1: 2]) {
      translate([i * 10, j * 10, -10])
      cube([6, 6, 0.2], center=true);

      translate([i * 10, j * 10, -5.5])
      cube([0.6, 0.6, 9], center=true);



     }
     }
*/
     translate([d1*0.5, 0, 0])
     cube([h1, d2, h1], center=true);
     translate([-d1*0.5, 0, 0])
     cube([h1, d2, h1], center=true);
     translate([0, d2*0.5, 0])
     cube([d1, h1, h1], center=true);
     translate([0, -d2*0.5, 0])
     cube([d1, h1, h1], center=true);
  }


 }
}

