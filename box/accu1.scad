  $fs = 0.2;

  h1 = 0.6;
  h0 = 1.0;

  h2 = 5;

// AAA: 11x45
/*
  // X
  n1 = 4;
  // Y
  n2 = 4;
  m1 = 4;
  d1 = 11;
  d2 = 45+m1;
//  d2 = 15;
//  t1 = 1.0;
  t1 = 0.8;
//*/

// AA: 15x52
/*
  n1 = 3;
  n2 = 3;
  m1 = 3;
  d1 = 15;
  d2 = 52+m1;
  t1 = 1;
//*/

// 18650
//*
  n1 = 3;
  n2 = 3;
  m1 = 4;
  d1 = 19;
  d2 = 66+m1;
  t1 = 1.2;
//*/


// AAA: 11x45

color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
   // BOTTOM:
   bottom = 1;
   // TOP:
   //top = 1;


  if (!is_undef(bottom)) {

  difference() {
    union() {
    // plus
      for(x=[0:1:n1-2])
      {
        translate([d1+x*(d1+h1),
          n2*(d1+h1)*0.5-h1, 0])
        cube([h1, n2*(d1+h1)-h1, d2],
          center=true);
      }
      for(y=[0:1:n2-2])
      {
        translate([n1*(d1+h1)*0.5-h1,
          d1+y*(d1+h1),  0])
        cube([n1*(d1+h1)-h1, h1, d2],
          center=true);
      }
    }

    // minus
    translate([0, 0, -d1*1.2])
    {
    for(i=[0:1:n1-1])
    {
      translate([d1*.5+(d1+h1)*i-h1*0.5,
        n2*d1, h2+d1])
      rotate([90, 0, 0])
      cylinder(d1*n2, d1*0.45, d1*0.45);

      translate([d1*.5+(d1+h1)*i-h1*0.5,
        n2*d1, h2-d1*0])
      rotate([90, 0, 0])
      cylinder(d1*n2, d1*0.45, d1*0.45);

      translate([d1*.5+(d1+h1)*i-h1*0.5,
        n2*d1, h2-d1*1])
      rotate([90, 0, 0])
      cylinder(d1*n2, d1*0.45, d1*0.45);
    }
    for(i=[0:1:n2-1])
    {
      translate([0, i*(d1+h1)+d1*0.5-h1*0.5,
        h2+d1])
      rotate([0, 90, 0])
      cylinder(d1*n1, d1*0.45, d1*0.45);

      translate([0, i*(d1+h1)+d1*0.5-h1*0.5,
        h2-d1*0])
      rotate([0, 90, 0])
      cylinder(d1*n1, d1*0.45, d1*0.45);

      translate([0, i*(d1+h1)+d1*0.5-h1*0.5,
        h2-d1*1])
      rotate([0, 90, 0])
      cylinder(d1*n1, d1*0.45, d1*0.45);
    }
    }
    translate([(d1+h1)*n1*0.5-h1,
        (d1+h1)*n2*0.5-h1, d2*0.6])
     cube([(d1+h1)*n1, (d1+h1)*n2, d2-10],
       center=true);
  }



//*
    translate([(n1*(d1+h1))*0.5-h1,
      (n2*(d1+h1))*0.5-h1, 0])
    difference() {
      cube([n1*(d1+h1)-h1+2*t1, 
        n2*(d1+h1)-h1+2*t1, d2],
        center=true);
      cube([n1*(d1+h1)-h1, n2*(d1+h1)-h1, d2+8],
        center=true);
    }
    translate([n1*(d1+h1)/2-h1, n2*(d1+h1)/2-h1,
        -d2/2-h0/2])
    cube([n1*(d1+h1)-h1+2*t1,
      n2*(d1+h1)-h1+2*t1, h0],
      center=true);


//*/

  }


  if (!is_undef(top)) {
    d0 = 2;
    difference() {
     translate([0, 0, 0])
     cube([d1+3*h1+1, d2+3*h1, d0], center=true);


     translate([0, 0, h1+5])
     cube([d1+h1+1, d2+h1, d0+10], center=true);
    }

    d = .2;
    translate([0, d2/2+d, 0.8])
    cube([d1-10, d, d*2], center=true);
    translate([0, -d2/2-d, 0.8])
    cube([d1-10, d, d*2], center=true);
  }


 }
}

