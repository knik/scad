color([1, 0.4, 0.1]) {
    translate([9, 0, 4.25])
    cube([18, 4.5, 8.5], center=true)
    ;

    translate([9, 0, 9])
    cube([18, 10, 1], center=true)
    ;

    translate([15, -1.1, 9])
    cube([12, 2.2, 10])
    ;
    
    translate([26, 0, 9])
    cylinder(10, 1.9, 1.9, $fs=0.1)
    ;

    translate([14, 0, 9])
    cylinder(10, 3.5, 3.5, $fs=0.1)
    ;
    
    
    
}
        
//
