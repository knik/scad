// 43
// 11
$fs=0.2;
color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
  r1 = 11/2;
  r2 = 43/2;

  translate([0, -7, 20])
  cylinder(5, r1, r1);

  
  difference() {
    translate([0, -7, 18])
    cylinder(10, r1+2, r1+2);


    rotate([90, 0, 0])
    difference() {
      translate([0, 0, -1])
      cylinder(20, r2+2, r2+2);

    }
  }
}
}
//
