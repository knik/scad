$fs=0.2;

//r1 = 38*0.5;
//r1 = 37.8*0.5;
r1 = 37.4*0.5;
//r2 = 50*0.5;
r2 = 44*0.5;

partA = 1;
partB = 1;


color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
  d1 = 4;
  t1 = 0.8;
  d2 = 8;

  if (!is_undef(partA)) {
   translate([0, 0, 0])
   difference() {
    cylinder(d1, r1+t1, r1+t1, center = true);
    cylinder(12, r1, r1, center = true);
   }

/*
  rotate([0, 90, 0])
  translate([-5, 36.8+d2, 0])
  difference() {
    cylinder(t1, r1+d2, r1+d2, center = true);
    cylinder(12, r1, r1, center = true);

    translate([-45, 0, 0])
     cube([80, 80, 80],  center = true);
  }
  */
  
  /*
  translate([0, 44, 0])
   cube([2, 50, d1],  center = true);
  translate([0, 72, 23])
   cube([2, 6, 50],  center = true);
  */

/*    
    translate([0, 19.0+d2*0.5, 0]) {
     difference() {
      cube([1, d2, d1],  center = true);
     rotate([0, 90, 0])
     cylinder(20, 1, 1, center = true);
     }
   }
   */
   translate([0, 19.0+d2*0.5, 0])
    cube([1, d2, d1],  center = true);
//   translate([0, 21.0+d2*0.5, 6])
//    cube([1, d2*.5, 8],  center = true);
  }
  if (!is_undef(partB)) {
   //rotate([0, 0, 0])
   //translate([-5, 36.8+d2, 0])
   difference() {
     cylinder(t1, r2+d2, r2+d2, center = true);
     cylinder(12, r2, r2, center = true);

     translate([-48, 0, 0])
      cube([80, 80, 80],  center = true);

//     translate([0, 22.5, 0])
//     cylinder(20, 1, 1, center = true);
   }


/*
   translate([0, 19+d2*0.5, 20])
    cube([2, d2, d1],  center = true);
   translate([0, 21.0+d2*0.5, 26])
    cube([2, d2*.5, 8],  center = true);
*/


  }

  
}
}
//
