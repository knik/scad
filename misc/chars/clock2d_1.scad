module clk1 (a1, t1) {
  rotate([0, 0, -30*a1])
  translate([150, 0])
    rotate([0, 0, 30*a1])
    translate([0, 0])
    text(t1, size = 35, halign="center",
    valign = "center", spacing=1.0);
}


color([1, 0.4, 0.1]) {
//rotate([90, 0, 90])
  {
$fs=0.2;

 clk1(10, "1");
 clk1(11, "2");
 clk1(0, "3");
 clk1(1, "4");
 clk1(2, "5");
 clk1(3, "6");
 clk1(4, "7");
 clk1(5, "8");
      
 clk1(6, "9");
 clk1(7, "10");
 clk1(8, "11");
 clk1(9, "12");

// r0 = 180;
 r0 = 115;
 r1 = r0 - 10;
 //circle(1);
 r2 = 178;


 difference() {
  circle(r1+0.4);
  circle(r1);
 }
 difference() {
  circle(r2+0.4);
  circle(r2);
 }

 for(i=[0: 30: 360]) {
   rotate([0, 0, i])
   translate([0, r0])
   circle(2);
 }
 for(i=[0: 6: 360]) {
   rotate([0, 0, i])
   translate([0, r0])
   circle(1);
 }
}
}