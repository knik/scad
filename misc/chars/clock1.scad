module clk1 (a1, t1) {
  rotate([0, 0, -30*a1])
  translate([150, 0, 0])
    rotate([0, 0, 30*a1])
    translate([0, 0, -0.5])
    //scale([1, 1, 0.2])
    linear_extrude(2)
    text(t1, size = 30, halign="center",
    valign = "center", spacing=1.0);
}


color([1, 0.4, 0.1]) {
//rotate([90, 0, 90])
  {
$fs=0.2;


difference() {
 cylinder(0.6, d=350);
 clk1(10, "1");
 clk1(11, "2");
 clk1(0, "3");
 clk1(1, "4");
 clk1(2, "5");
 clk1(3, "6");
 clk1(4, "7");
 clk1(5, "8");
 clk1(6, "9");
 clk1(7, "10");
 clk1(8, "11");
 clk1(9, "12");
 translate([0, 0, -0.5])
  cylinder(2, d=250); 

 translate([0, 0, -1]) cube([200, 200, 4]);
 //translate([-200, 0, -1]) cube([200, 200, 4]);
 translate([-200, -200, -1]) cube([200, 200, 4]);
 translate([0, -200, -1]) cube([200, 200, 4]);

}
}
}