color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
      d4 = 0.4;
      d1 = 0.4;
      d2 = 60;
      d3 = 20;
      
      translate([0, 0, 0])
      cube([d2, d3, d1]);
  }
}
