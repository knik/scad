$fs=0.2;

r1 = 1;
r2 = 2;
d1 = 2;
d2 = 2;
t1 = 0.2;

color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
    cylinder(d1, r1, r1, center=true);
    translate([0, 0, -d2])
      cylinder(d1, r2, r2, center=true);
    translate([0, 0, d2])
      cylinder(d1, r1+t1, r1+t1, center=true);
  }
}


