// 31.8

    $fs = 0.2;

//    r1 = 31.2/2;
    r1 = 31.8/2;
    h1 = 4;
    d1 = 2.4;

module w1(r1, d1)
{
    difference() {
     cylinder(h1, r1+d1, r1+d1, center=true);

    //translate([0, 0, 1])
     cylinder(h1+1, r1, r1, center=true);
    }
}


color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
    translate([0, 0, 0])
      w1(r1 + .03, d1 - 0.2);
    translate([40, 0, 0])
      w1(r1 + .06, d1 - 0.4);
  }
}


