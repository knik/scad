color([1, 0.4, 0.1]) {
rotate([0, 0, 45])
  {
      r1 = 3;
      d1 = 2;
      d2 = 38;
      d3 = 6;
      //d4 = 50;
      d4 = 5;
      d5 = 1;
      d6 = 3.6;
      d7 = 2.5;
      
      translate([0, 0, 0])
      //cube([d2, d3, d1]);
      cube([d2, d3, d1]);

      translate([0, 0, 0])
      cube([d3, d2, d1]);
      
      translate([0, 0, 0])
      cube([d5, d2, d4]);

      translate([0, 0, 0])
      cube([d2, d5, d4]);


      translate([d7, d7, 0])
      cube([d5, d2-3, d4]);

      translate([d7, d7, 0])
      cube([d2-3, d5, d4]);
      
      translate([d2-1, 0, 0])
      cube([d5, d3, d4]);

      translate([0, d2-1, 0])
      cube([d3, d5, d4]);
  }
}
