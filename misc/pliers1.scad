  //hand = 1;
  box1 = 1;


$fs=0.2;

r1 = 3.8;
d1 = 1.2;

color([1, 0.4, 0.1]) {
rotate([90, 0, 90])
  {
    if (!is_undef(hand)) {
      for(i=[0:1:16])
      {
        rotate([0, i*3, 0])
        translate([0, 0, 80])
        rotate([0, 90, 0])
        difference() {
          cylinder(5, r1+d1, r1+d1, center=true);
          cylinder(20, r1, r1, center=true);
        }
      }
      translate([-r1+2, 0, 80])
      difference() {
          sphere(r1+d1);
          sphere(r1);
          translate([10, 0, 0])
          cube([20, 20, 20], center = true);
      }
    }
    if (!is_undef(box1)) {
      d1 = 3;
      difference() {
        cube([8.4 + d1, 8.4 + d1, 10], center = true);
        cube([8.4, 8.4, 20], center = true);
      }
    }
  }
}


