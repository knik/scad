$fs = 0.2;

color([1, 0.4, 0.1]) {
rotate([0, 50, 0])
  {
    r1 = 12.0;
    d1 = 0.8;
    d2 = 6;
    d3 = 8;

    rotate([-60, 0, 0])
    difference() {
      sphere(r1+d1);
      sphere(r1);
      translate([0, 0, 1.1*r1])
       cube([4*r1, 4*r1, 4*r1], center = true);
    }
    //translate([0, 8.9*d2, -1.8*d2])
    translate([0, 8.8*d2, -1.8*d2])
     cube([d3, 20*d2, d1], center = true);
    translate([0, 8.8*d2, -d3*1.6])
     cube([d1, 20*d2, d3*.6], center = true);
  }
}
