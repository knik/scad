grid = 0.2;
d1 = 40;
d2 = 10;
d3 = 14;
r1 = 2.5;
d4 = 0.8;

module pip1()
{
    difference() {
     translate([0, 0, 0])
      cylinder(d2, r1+d4, r1+d4, $fs=grid);

     translate([0, 0, -1])
      cylinder(d2+10, r1, r1, $fs=grid);
    }
}


color([1, 0.4, 0.1]) {
  {
rotate([90, 0, 0])
  if (0) {
    translate([0, -3, d1-10])
     cube([d3+d4, d4, d1+6], center=true);
    translate([d3/2, -1.7, d1-10])
     cube([d4, d4+2.5, d1+6], center=true);
    translate([-d3/2, -1.7, d1-10])
     cube([d4, d4+2.5, d1+6], center=true);

    translate([4.5, -1.6, 8])
     cube([4, 3, d4], center=true);
    translate([-4.5, -1.6, 8])
     cube([4, 3, d4], center=true);

    translate([4.5, -1.6, d1+12])
     cube([4, 3, d4], center=true);
    translate([-4.5, -1.6, d1+12])
     cube([4, 3, d4], center=true);

    intersection() {
      translate([0, -10, 20])
       cube([20, 20, d1*2], center=true);

      union() {
       pip1();
        translate([0, 0, d1+d2])
         pip1();

      }
    };
   }
  if (1) {
    difference() {
     translate([0, 0, 0])
      cylinder(1, 5, 5, $fs=grid);

     translate([0, 0, -5])
      cylinder(10, 3.5, 3.5, $fs=grid);
    }
   }

  }
}

