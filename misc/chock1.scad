color([1, 0.4, 0.1]) {
rotate([90, 0, 0])
  {
    l1=20;
    l2=20;
    a1=15;
    s1=60;
    th1=2;
      
   rotate([0, 0, a1/2])
   translate([0, 0, 0])
    cube([th1, l2, l1], center=true);



   difference() {
    union() {
      translate([s1/2, 0, -10])
       cube([s1+5, l2, 1], center=true);
     translate([s1/2, 0, 10])
      cube([s1+5, l2, 1], center=true);


    }

    rotate([0, 0, -a1/2])
    translate([s1+5, 10, 0])
    cube([10, l2+10, l1+10], center=true);

    rotate([0, 0, a1/2])
    translate([-5.5, 0, 0])
    cube([10, l2+10, l1+10], center=true);



   }

   translate([s1, 0, 0])
   rotate([0, 0, -a1/2])
    cube([th1, l2, l1], center=true);


      
  }
}
