// 240 x 140 x 70 mm
// 120 x 140 x 70 mm

$fs = 0.2;

color([1, 0.4, 0.1]) {
rotate([90, 0, 0])
  {
    //h1 = 0.2;
    h1 = 0.2;
    h2 = 0.6;

    //d1 = 240;
    d1 = 130;
    //d2 = 140;
    d2 = 66;

    t0 = 0.8;
    //t1 = 1.0;
    t1 = 0.8;
    //t2 = 10;
    t2 = 10;


      for(i=[-d1*0.5:t1+t2:d1*0.5])
      {
        translate([i, -h2+h1, 0])
        cube([t0, d2, h2], center=true);
      }
      for(i=[-d2*0.5:t0+t1:d2*0.5])
      {
        translate([0, i, 0])
        cube([d1, t1, h1], center=true);
      }
  }
}
