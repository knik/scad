d1 = 0.6;
d12 = 0.6;
d2 = 100;
d3 = 60;
d4 = 20;
grid = 0.2;
r1 = 2.6;

module p1() {
  d = 1;
  l = 10;
  translate([0, 0, 0])
  cube([d4, d, l]);
  translate([0, 0.4 + d1 + d1 + 0.2, 0])
  cube([d4, d, l]);
}

color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
  if (0) {
   translate([0, 0, 0])
   cube([d3, d2, d1]);
  }
  if (1) {
    translate([0, 0, 0])
      p1();
    translate([0, d2-4, 0])
      p1();

    difference() {
      cube([d4, d2-d1*2, d1]);
      translate([10, d2/2, 0])
        cylinder(d4/2, r1, r1, $fs=grid, center=true);
    }

    translate([0, 2, d1])
    cube([d1, d2-6, 2]);
    translate([d4-d1, 2, d1])
    cube([d1, d2-6, 2]);
  }
  
  
  }
}

