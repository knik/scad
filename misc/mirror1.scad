color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
      d1 = 1;
      d2 = 10;
      d3 = 40;
      d4 = 45;
      d5 = 2.4;
      d6 = 6;
      
      translate([0, 0, 0])
      cube([d1, d3, d2]);

      translate([0, d3/2-d6/2, 0])
      cube([d4, d6, d1]);

      translate([d4, 0, 0])
      cube([d6, d3, d1]);
      
      translate([-d5, 0, d2-d1])
      cube([d5, d6, d1]);
      translate([-d5, d3-d6, d2-d1])
      cube([d5, d6, d1]);
      
      translate([-d5, 0, 0])
      cube([d1, d6, d2]);
      translate([-d5, d3-d6, 0])
      cube([d1, d6, d2]);

      
  }
}
