$fs = 0.2;

color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
    r1 = 23.6;
    r1b = 25.0;
    r2 = 16.2;
    h1 = 8;
    h2 = 1.6;

    difference() {
      cylinder(h2, r1b, r1b, center=true);
      cylinder(h1+1, r2, r2, center=true);
    }

    translate([0, 0, h1*0.5])
    difference() {
      cylinder(h1, r1, r1, center=true);
      cylinder(h1+1, r2, r2, center=true);
    }

  }
}
