$fs = 0.2;

//partA = 1;
partB = 1;

color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
    d0 = 0.1;
   
    //h1 = 0.4;
    h1 = 0.6;

    //h2 = 5;
    h2 = 8;

    h3 = 12;
//    //h3 = 4;
    h4 = 0.75;

    //d1 = 54;
    //d1 = 57;
    d1 = 46;
    //d1 = 14;

    t1 = 0.6;
    t2 = 0.4;

//    d2 = 3.0;
    d2 = 4.0;


//    d3 = 13.5;
//    d3 = 14.0;
    d3 = 14.6;

   if (!is_undef(partA)) {
    intersection() {
      cylinder(10, 0.5*d1, 0.5*d1, center = true);

    difference() {
      union() {
      for(i=[-d1*0.5:d2:d1*0.5])
      {
        translate([i, 0, 0])
        cube([t1, d1, h1], center=true);
      }
      for(i=[-d1*0.5:d2:d1*0.5])
      {
        translate([0, i, 0])
        cube([d1, t1, h1], center=true);
      }
      }
      cylinder(20, 0.5*d3, 0.5*d3, center = true);
    }

    }

    translate([0, 0, h2*0.5-h1*0.5])
    difference() {
      cylinder(h2, 0.5*d3+d0, 0.5*d3+d0, 
        center = true);
      cylinder(20, 0.5*d3-t2, 0.5*d3-t2, 
        center = true);
    }
    translate([0, 0, 0])
    difference() {
      cylinder(h1, 0.5*d1+d0, 0.5*d1+d0, 
        center = true);
      cylinder(20, 0.5*d1-t2, 0.5*d1-t2, 
        center = true);
    }
   }
   if (!is_undef(partB)) {
    difference() {
      cylinder(h3, 0.5*d3-h4, 0.5*d3-h4, 
        center = true);
      cylinder(20, 0.5*d3-h4-0.6, 0.5*d3-h4-0.6, 
        center = true);

    }
    translate([0, 0, -h3*0.5])
    cylinder(0.6, 0.5*d3-h4, 0.5*d3-h4, 
      center = true);
   }
  }
}
