$fs=0.2;

partA = 1;

color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
  d1 = 0.6;
  d2 = 15;
  d3 = 12;
  d4 = 3;

  if (!is_undef(partA)) {
    translate([0, 0, 0])
    cube([d2, d3, d1],  center = true);

    translate([0, d3*0.5-.3, d4*0.5])
    cube([d2, d1, d4],  center = true);


/*    
    difference() {
      cube([d3-h2, d3-h2, d2],  center = true);
      cylinder(12, r1, r1, center = true);
    }
    translate([d1*0.5+h1*0.5, 0, 0])
    cube([d1, d4, d2],  center = true);

    translate([-d1*0.5-h1*0.5, 0, 0])
    cube([d1, d4, d2],  center = true);
*/
  }
  if (!is_undef(partB)) {

    difference() {
      cube([d3+d6, d3+d6, d2],  center = true);
      cube([d3, d3, d2+10],  center = true);
    }
    translate([d5, 0, 0])
    cube([d5+2, d4, d2],  center = true);

    translate([-d5, 0, 0])
    cube([d5+2, d4, d2],  center = true);

  }
  if (!is_undef(partC)) {
//    r1= 3.8 * 0.5;
    r1= 3.75 * 0.5;
    r2= 5;
    r3= 6;
    cylinder(3, r1, r1);
    translate([0, 0, -1])
    cylinder(1.4, r2, r2);
    translate([0, 0, -3])
    cylinder(2, r3, r3);
  }
  }
}
