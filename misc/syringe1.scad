$fs = 0.2;

color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
    r1 = 2.16;
    d1 = 8;
    t1 = 1;

    translate([0, 0, d1*.5])
    difference() {
      cylinder(d1, r1+t1, r1+t1, center = true);
      cylinder(d1+2, r1, r1, center = true);
    }
    cylinder(2, r1+t1, r1+t1, center = true);
  }
}
