  pipe1 = 1;


$fs=0.2;

r1 = 5.8*0.5;
r2 = 9*0.5;
d1 = 23;
d2 = d1+6.6;
t1 = 0.6;

color([1, 0.4, 0.1]) {
rotate([0, 0, 90])
  {
    if (!is_undef(pipe1)) {
        difference() {
          cylinder(d2, r2, r2, center=true);
          cylinder(40, r1, r1, center=true);
          translate([0, 0, 3.2-d2*0.5+d1])
           cube([20, 2, 5], center = true);
          translate([0, 0, 25.8-d2*0.5+d1])
           cylinder(40, r2-t1, r2-t1, center=true);
          translate([0, 0, 25.8-d2*0.5+d1])
           cylinder(40, r2-t1, r2-t1, center=true);
          translate([0, 0, 5.8-d2*0.5+d1])
           cube([20, 0.6, 6], center = true);
        }
    }
  }
}


