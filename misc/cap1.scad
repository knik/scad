color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
    grid = 0.2;

//    d1 = 3.6;
    d1 = 3.8;
    d2 = 0.6;
    d3 = .4;
    h1 = 8;

    difference() {
     cylinder(h1, d1+d2, d1+d2+d3, center=true, $fs=grid);

    translate([0, 0, 1])
     cylinder(h1, d1, d1+d3, center=true, $fs=grid);
    }
  }
}
