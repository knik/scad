d1 = 12.4;
d2 = 4.8;
h1 = 10;

t1 = 0.6;

$fs=0.2;
color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
  translate([0, 0, 0])
  cube([t1, d1, h1],  center = true);

  translate([d2+t1, 0, 0])
  cube([t1, d1, h1],  center = true);

  translate([d2*0.5+t1*.5, -d1*0.5-t1*0.5, 0])
  cube([d2+t1*2, t1, h1],  center = true);

  translate([d2*0.5+t1*.5, d1*0.5+t1*0.5, 0])
  cube([d2+t1*2, t1, h1],  center = true);

  translate([d2*0.5+t1*.5, 0, -h1*0.5+t1*0.5])
  cube([d2, d1, t1],  center = true);


}
}
//
