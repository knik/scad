// meat lid: d=145; (spoon hole: 14/6mm)

color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
translate([30, 0, 0])
  {
    //r1 = 72.5;
    r1 = 73.5;
    //d2 = 12;
    d2 = 8;
    d3 = 0.4;

    d1 = 0.8;
    d4 = 0.6;

    difference() {
    union() {

    difference() {
      cylinder(d2, r1 + d1, r1 + d1);
      translate([0, 0, d4])
      cylinder(d2, r1, r1);
    }

    difference() {
      cylinder(2, r1 + d1, r1 + d1);
      translate([0, 0, d4])
      cylinder(d2, r1-1, r1-1);
    }

    translate([0, 0, d2 + 0])
    difference() {
      cylinder(d1, r1 + d1, r1 + d1);
      translate([0, 0, -4])
      cylinder(10, r1 - d3, r1 - d3);
    }
    }

    translate([68, -7, -2])
    cube([10, 14, 20]);

   }

  }
}
