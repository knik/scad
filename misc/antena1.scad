color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
      d0 = 0.6;
      d00 = 0.4;
      d1 = 40;
      d2 = 120;
      d3 = 2;
      d4 = 2;
      d5 = 80;
      d6 = 4;
      d7 = 140;

    if (0) {
      translate([0, 0, 0])
      cube([d2, d1, d0]);

      translate([0, 0, 0])
      cube([d0, d1, d3]);

      translate([d2, 0, 0])
      cube([d0, d1, d3]);
    }

    if (0) {
     difference() {
      union() {
        translate([0, 0, 0])
        cube([d2 - 2*d0, d1, d00]);

        translate([0, 0, 0])
        cube([d0, d1, d4]);

        translate([d2 - 2*d0, 0, 0])
        cube([d0, d1, d4]);

        translate([0, 0, 0])
        cube([d2-2*d0, d0, d4]);

        translate([0, d1 - d0, 0])
        cube([d2-2*d0, d0, d4]);
      }
      translate([d2/2, d1/2-5, -5])
      cube([10, 10, 10]);
     }
     }
      
    if (1) {
      translate([0, 0, 0])
      cube([d7, d5, d0]);

      translate([0, 0, 0])
      cube([d0, d5, d6]);

      translate([d7, 8, 0])
      cube([d0, d5-16, d6]);
    }
      
  }
}
