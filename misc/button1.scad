$fs=0.2;
color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
  r1 = 2.4;
  d1 = 0.6;
  h1 = 1;
  h2 = 1;

   difference() {
     cylinder(h1+h2, r1+d1, r1+d1, center=true);
     translate([0, 0, 5])
     cylinder(10+h1, r1, r1, center=true);
   }



 }
}

