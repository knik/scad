color([1, 0.4, 0.1]) {
//rotate([30, 0, 0]) 
{
    l1=12;
    d1=0.9;
    d2=1.8;
    h1=1.5;
    h2=2;
    l2=18;
    l3=12;
    d3=1.5;

    translate([0, 0, 0])  
    cylinder(h1, d2, d2, center=true, $fs=0.1);

    translate([0, l3/2-3, (h1+h1)/2])
    cube([5, l3, h1], center=true);

    translate([0, -l2/2+4, (-h2-h1)/2])
    cube([5, l2, h2], center=true);

    difference() {
     translate([0, 7, d3/2-h1/2])
     cube([5, 14, d3], center=true);

     translate([0, l1-0.2, 0])
     cylinder(20, d1, d1, center=true, $fs=0.1);
     translate([0, l1, 0])
     cylinder(20, d1, d1, center=true, $fs=0.1);
    }
}
}
        
//
