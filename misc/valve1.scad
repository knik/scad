 // r = 8.6
// h = 6.8
$fs=0.2;


//partA = 1;
//partB = 1;
partC = 1;

color([1, 0.4, 0.1]) {
rotate([0, 0, 45])
  {
//  r1 = 8.3*0.5;
//  h1 = 6.4*0.5;
  r1 = 8.4*0.5;
//  h1 = 6.0;
  h1 = 6.6;
  d1 = 1;
  d2 = 6;
//  d3 = 2;
//  d4 = 6;
//  d5 = 12;
//  d3 = 5;
  d3 = 11;
  d4 = 8;
  d5 = 14;
  d6 = 4;
  h2 = 0.44;


  if (!is_undef(partA)) {
    difference() {
      cube([d3-h2, d3-h2, d2],  center = true);
      cylinder(12, r1, r1, center = true);
    }
    translate([d1*0.5+h1*0.5, 0, 0])
    cube([d1, d4, d2],  center = true);

    translate([-d1*0.5-h1*0.5, 0, 0])
    cube([d1, d4, d2],  center = true);
  }
  if (!is_undef(partB)) {

    difference() {
      cube([d3+d6, d3+d6, d2],  center = true);
      cube([d3, d3, d2+10],  center = true);
    }
    translate([d5, 0, 0])
    cube([d5+2, d4, d2],  center = true);

    translate([-d5, 0, 0])
    cube([d5+2, d4, d2],  center = true);

  }
  if (!is_undef(partC)) {
//    r1= 3.95 * 0.5;
    r1= 4.0 * 0.5;
    r2= 5;
    r3= 6;
    cylinder(4, r1, r1);
    translate([0, 0, -1])
    cylinder(1.4, r2, r2);
    translate([0, 0, -3])
    cylinder(2, r3, r3);
  }
  }
}
