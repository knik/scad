// 110 x 46

   $fs=0.2;


   s1=1.5;
   s2=1.0;
//   r1=6.35;
   s3=1.4;
   r1=2.2;
   r2=r1-s3;
   r1n = 3.4;


   t1 = 8;

srub = 1;
//nakr = 1;
//bridge = 1;

module scr1 (a1) {
  rotate([0, 0, 24*a1])
  translate([r1, 0, 1+a1*s1/15])
  rotate([0, 45, 0])
  cube([s2, 1.25, s2], center=true);
}
module scr2 (a1) {
  rotate([0, 0, 24*a1])
  translate([r1+1.1, 0, 1.8+a1*s1/15])
  rotate([0, 45, 0])
  cube([s2, 1.7, s2], center=true);
}

module srub1 () {
   translate([0, 0, 14])
    cylinder(30, r1, r1, center=true);

   for(i=[0:1:14] )
   {
    translate([0, 0, t1+s1*0]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*1]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*2]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*3]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*4]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*5]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*6]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*7]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*8]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*9]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*10]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*11]) {
     scr1(i);
    }
    translate([0, 0, t1+s1*12]) {
     scr1(i);
    }
   }
}


color([1, 0.4, 0.1]) {
//rotate([180, 0, 0])
rotate([0, 0, 90])
  {
  if (!is_undef(srub)) {
   translate([0, 0, 0])
    srub1();

   translate([46, 0, 0])
    srub1();

   //translate([46, 0, -2])
   // cube([6, 10, 2], center=true);
   //translate([0, 0, -2])
   // cube([6, 10, 2], center=true);

   translate([23, 0, -1])
    cube([51, 4.5, 1.0], center=true);

/*
   translate([0, 4, 0.5])
    cube([0.6, 40, 4], center=true);
   translate([46, 4, 0.5])
    cube([0.6, 40, 4], center=true);
*/
  }
  if (!is_undef(nakr)) {
   difference() {
   d = 1;
   translate([0, 0, 4])
    cylinder(6, r1n+d, r1n+d, center=true);
    cylinder(40, r1n, r1n, center=true);
   }

   for(i=[0:1:14] )
   {
    translate([0, 0, s1*0]) {
     scr2(i);
    }
    translate([0, 0, s1*1]) {
     scr2(i);
    }
    translate([0, 0, s1*2]) {
     scr2(i);
    }
    translate([0, 0, s1*2]) {
     scr2(i);
    }
   }
   translate([5.4, 0, 3])
     cube([4, 4, 4], center=true);
   translate([-5.4, 0, 3])
     cube([4, 4, 4], center=true);
  }
  if (!is_undef(bridge)) {
   d = 1;
   difference() {
   translate([0, 0, 0])
    cube([110, 10, 4], center = true);
    translate([-23, 0, 0])
     cylinder(40, 3.3, 3, center=true);
    translate([23, 0, 0])
     cylinder(40, 3.3, 3, center=true);
    translate([54, 5, 0])
     cube([50, 5, 8], center = true);
    translate([54, -5, 0])
     cube([50, 5, 8], center = true);
    translate([-54, 5, 0])
     cube([50, 5, 8], center = true);
    translate([-54, -5, 0])
     cube([50, 5, 8], center = true);
    translate([0, -5, 0])
     cube([34, 5, 8], center = true);
    translate([0, 5, 0])
     cube([34, 5, 8], center = true);
   }
   translate([56, 0, 3])
    cube([2, 5, 10], center = true);
   translate([-56, 0, 3])
    cube([2, 5, 10], center = true);

//   translate([5.4, 0, 3])
//     cube([4, 4, 4], center=true);
//   translate([-5.4, 0, 3])
//     cube([4, 4, 4], center=true);
  }
 }
}


