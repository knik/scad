r1=2.6/2;
d1=0.6;

color([1, 0.4, 0.1]) {
//color([.3, .6, 0.3]) {
rotate([180, 0, 0])
  {

   $fs=0.2;

   translate([0, 0, 0])
   difference()
   {
     cylinder(12, r1+d1, r1+d1, center=true);
     cylinder(30, r1, r1, center=true);
   };

  }
}


