// 19.8/22.2

    $fs = 0.1;

    r1 = 20/2;
    r2 = 22.2/2;
    h1 = 1;

module w1()
{
    difference() {
     cylinder(h1, r2, r2, center=true);
     cylinder(h1+1, r1, r1, center=true);
    }
}


color([1, 0.4, 0.1]) {
rotate([0, 0, 0])
  {
    translate([0, 0, 0])
      w1();
//    translate([30, 0, 0])
//      w1();
  }
}


