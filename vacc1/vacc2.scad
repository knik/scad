$fs=0.2;

M1=1;
//R2=1;

//r1 = 40.0*0.5;
r1 = 42.0*0.5;
r2 = 44.0*0.5;
d1 = 2.0;
d2 = 20.0;

module scr1 (a1) {
  r = 0.8;
  for(i=[0:1:a1] )
   {
    rotate([-4, 0, 10*i])
    translate([r1, 0, 4*i/36])
    rotate([95, 0, 0])
    cylinder(3.8, r, r, center=true);

    translate([0, 0, 4])
    rotate([-4, 0, 10*i])
    translate([r1, 0, 4*i/36])
    rotate([95, 0, 0])
    cylinder(3.8, r, r, center=true);

/*
    translate([0, 0, 8])
    rotate([-4, 0, 10*i])
    translate([r1, 0, 4*i/36])
    rotate([95, 0, 0])
    cylinder(3.8, r, r, center=true);
*/

   }
}

color([1, 0.4, 0.1]) {
//color([.3, .6, 0.3]) {
rotate([180, 0, 0])
  {

  if (!is_undef(M1)) {

    translate([0, 0, -d2*0.5-d1*0.5])
    difference()
    {
      cylinder(d1, r2-2, r2, center=true);
      cylinder(50, r1, r1, center=true);
    }
    difference()
    {
      cylinder(d2, r2, r2, center=true);
      cylinder(50, r1, r1, center=true);
    }
    
    translate([0, 0, d2*0.5-0])
    difference()
    {
      cylinder(1, r2+2, r2+2, center=true);
      cylinder(50, r1, r1, center=true);
    }

    rotate([1, 0, 0])
    intersection() {
     translate([0, 0, d2-14])
     difference()
     {
       cylinder(1, r2+1, r2+1, center=true);
       cylinder(50, r1, r1, center=true);
     }
     cube([50, 20, 50], center=true);
    }

    translate([0, 0, 15])
    scr1(35);

    translate([0, 0, d2*0.5+8])
    difference()
    {
      cylinder(16, r1+0.5, r1+0.5, center=true);
      cylinder(50, r1, r1, center=true);
    }

  }


if (!is_undef(R1)) {
   translate([0, 0, -5])
//   scr1(54);
   scr1(35);

   translate([0, 0, -5.8])
   difference()
   {
     cylinder(d3, r2, r2, center=true);
     cylinder(50, r1, r1, center=true);
   }
   translate([0, 0, 5.8])
   difference()
   {
     cylinder(d3, r2, r2, center=true);
     cylinder(50, r1, r1, center=true);
   }

   translate([0, 0, 5.2])
   difference()
   {
     cylinder(d3, r1+d3, r1+3, center=true);
     cylinder(50, r1, r1, center=true);
   }
   translate([0, 0, -5.2])
   difference()
   {
     cylinder(d3, r1+3, r1+d3, center=true);
     cylinder(50, r1, r1, center=true);
   }

   translate([0, 0, 0])
   difference()
   {
     cylinder(10, r1+d3, r1+d3, center=true);
     cylinder(50, r1, r1, center=true);
   }
  }

if (!is_undef(R2)) {
   translate([0, 0, -d1b*0.5-.2])
   difference()
   {
     cylinder(d3, r2, r2, center=true);
     cylinder(50, r1, r1, center=true);
   }
   translate([0, 0, d1b*0.5+.2])
   difference()
   {
     cylinder(d3, r2, r2, center=true);
     cylinder(50, r1-2, r1-2, center=true);
   }

   translate([0, 0, d1b*0.5-0.4])
   difference() 
   {
     cylinder(d3, r1+d3, r1+3, center=true);
     cylinder(50, r1, r1, center=true);
   }
   translate([0, 0, -d1b*0.5+0.4])
   difference() 
   {
     cylinder(d3, r1+3, r1+d3, center=true);
     cylinder(50, r1, r1, center=true);
   }

   translate([0, 0, 0])
   difference()
   {
     cylinder(d1b, r1+d3, r1+d3, center=true);
     cylinder(50, r1, r1, center=true);
   }
  }
 }
}
